public class FindPrimes {

    public static void main(String[] args) {
        int number = Integer.parseInt(args[0]);
        System.out.print("2");
        for (int i = 3; i <= number; i++) {
            if (isPrime(i)) {
                System.out.print("," + i);
            }

        }
    }

    public static boolean isPrime(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;

    }
}
